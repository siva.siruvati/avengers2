
FROM openjdk:8-alpine
ADD /build/libs/app-1.0.jar app.jar
ENTRYPOINT ["java", "-jar", "/app.jar"]

