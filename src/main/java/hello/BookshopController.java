package hello;

import hello.models.Book;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class BookshopController {
    @RequestMapping("/home")
    public Book greeting() {
        Book book = new Book("Book shop1");
        return book;
    }
}
